#[macro_use]
extern crate clap;
extern crate failure;
extern crate kvs;

use failure::format_err;
use failure::Error;

use clap::App;

fn do_get(kvs: &mut kvs::KvStore, options: &clap::ArgMatches) -> Result<(), Error> {
  use kvs::KvsError;
  // get key, this isn't optional so it's a bug for it not to present at this stage
  let key = options.value_of("key").unwrap();
  match kvs.get(key.to_string())? {
    Some(value) => println!("{}", value),
    None => println!("Key not found"),
  }
  Ok(())
}

fn do_set(kvs: &mut kvs::KvStore, options: &clap::ArgMatches) -> Result<(), Error> {
  let key = options.value_of("key").unwrap();
  let value = options.value_of("value").unwrap();
  kvs.set(key.to_string(), value.to_string())?;
  Ok(())
}

fn do_rm(kvs: &mut kvs::KvStore, options: &clap::ArgMatches) -> Result<(), Error> {
  use kvs::KvsError;
  let key = options.value_of("key").unwrap();
  match kvs.remove(key.to_string()) {
    Err(e@KvsError::KeyNotFound(_)) => {
      println!("Key not found");
      Err(e.into())
    },
    Err(e) => {Err(e.into())},
    Ok(()) => Ok(())
  }
}

fn run() -> Result<(), Error> {
  let cli_config = load_yaml!("cli.yml");
  let clap_app = App::from_yaml(cli_config)
    .author(crate_authors!(" & "))
    .about(crate_description!())
    .name(crate_name!())
    .version(crate_version!());

  let options = clap_app.get_matches();
  let mut kvs = kvs::KvStore::open(std::env::current_dir()?.as_path())?;

  if let Some(get_matches) = options.subcommand_matches("get") {
    do_get(&mut kvs, &get_matches)?;
  } else if let Some(set_matches) = options.subcommand_matches("set") {
    do_set(&mut kvs, &set_matches)?;
  } else if let Some(rm_matches) = options.subcommand_matches("rm") {
    do_rm(&mut kvs, &rm_matches)?;
  } else {
    eprintln!("{}", options.usage());
    return Err(format_err!("-V, --version, --help or a subcommand"));
  }
  Ok(())
}

fn main() {
  use failure::Fail;
  use std::process::exit;

  match run() {
    Err(err) => {
      eprintln!("Error: {}", err);
      exit(1);
    }
    Ok(_) => {}
  }
}
