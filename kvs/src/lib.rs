extern crate failure;
extern crate serde;
extern crate serde_json;
extern crate tempfile;

use failure::Fail;
use serde::{Deserialize, Serialize};

use std::collections::HashMap;
use std::fs::{File, OpenOptions};
use std::io::{Seek, Write, SeekFrom, BufReader, BufWriter};
use std::path::{Path, PathBuf};
use std::result::Result as StdResult;
use tempfile::tempdir;

#[derive(Debug, Serialize, Deserialize)]
enum LogCommand {
  Set(String, String),
  Remove(String),
}

#[derive(Fail, Debug)]
pub enum KvsError {
  #[fail(display = "Key '{}' not found", _0)]
  KeyNotFound(String),

  #[fail(display = "IO error: {}", _0)]
  IO(#[cause] std::io::Error),

  #[fail(display = "Read/Write error: {}", _0)]
  RW(#[cause] serde_json::Error),

  #[fail(display = "Log format error: {}", _0)]
  LogFormat(String)
}

pub type Result<T> = StdResult<T, KvsError>;

/// A in-memory key value store. The idea here
/// is simply to allow us to store some information with a specific
/// name, and then later ask about its value, change or remove it.
///
/// # Example
/// ```
/// use kvs::KvStore;
/// let mut kvs = KvStore::new();
/// kvs.set("something".to_string(), "else".to_string());
/// assert_eq!(kvs.get("something".to_string()), Some("else".to_string()));
/// assert_eq!(kvs.get("nothing".to_string()), None);
///
/// kvs.remove("something".to_string());
/// assert_eq!(kvs.get("something".to_string()), None);
/// ```
#[derive(Debug)]
pub struct KvStore {
  kv_cache: HashMap<String, SeekFrom>,
  log_file: File,
  log_file_path: PathBuf,
  compaction_counter: i32,
  enable_compaction: bool
}

impl KvStore {
  const N_COMPACTION_CHECKS: i32 = 100;
  fn open_internal(path: &Path, enable_compaction: bool) -> Result<KvStore> {
    let log_file_path = path.join(Path::new("log"));
    let mut file = std::fs::OpenOptions::new()
      .create(true)
      .read(true)
      .append(true)
      .open(log_file_path.as_path())
      .map_err(|e| KvsError::IO(e))?;
    let mut cache = HashMap::new();
    let mut compaction_counter = 0;
    file
      .seek(std::io::SeekFrom::Start(0))
      .map_err(|e| KvsError::IO(e))?;
    let mut buf_reader = BufReader::new(&file);
    'readLogItems: loop {
      compaction_counter += 1;
      let seekpos = buf_reader.seek(SeekFrom::Current(0))
        .map_err(|e| KvsError::IO(e))?;
      let mut de = serde_json::Deserializer::from_reader(&mut buf_reader);
      match LogCommand::deserialize(&mut de) {
        Err(ref e) if e.classify() == serde_json::error::Category::Eof => {
          break 'readLogItems;
        }
        Err(e) => Err(KvsError::RW(e)),
        Ok(LogCommand::Set(key, value)) => {
          cache.insert(key, SeekFrom::Start(seekpos));
          Ok(())
        }
        Ok(LogCommand::Remove(key)) => {
          cache.remove(&key);
          Ok(())
        }
      }?;
    }
    Ok(KvStore {
      kv_cache: cache,
      log_file: file,
      log_file_path,
      compaction_counter,
      enable_compaction
    })
  }

  pub fn open(path: &Path) -> Result<KvStore> {
    Self::open_internal(path, true)
  }

  /// Gets the value stored for the given key (if any)
  pub fn get(&mut self, key: String) -> Result<Option<String>> {
    if let Some(pos) = self.kv_cache.get(&key) {
      self.log_file.seek(*pos);
      let log_command = LogCommand::deserialize(&mut serde_json::Deserializer::from_reader(&mut BufReader::new(&self.log_file)))
        .map_err(|e| KvsError::RW(e));
      self.log_file.seek(SeekFrom::End(0));
      return match log_command? {
        LogCommand::Set(k, v) => {
          if k != key {
            Err(KvsError::LogFormat("key mismatch".to_string()))
          } else {
            Ok(Some(v))
          }
        }
        LogCommand::Remove(_) => {
            Err(KvsError::LogFormat("Found remove when expecting set".to_string()))
        }
      }
    }
    Ok(None)
  }

  /// Sets the value for the given key
  pub fn set(&mut self, key: String, value: String) -> Result<()> {
    let is_new_value_for_key = self.get(key.clone())?
      .map(|v| v != value)
      .unwrap_or(true);
    if is_new_value_for_key {
      let seek_pos = self.log_file.seek(SeekFrom::Current(0))
        .map_err(|e| KvsError::IO(e))?;
      serde_json::to_writer(&mut BufWriter::new(&self.log_file), &LogCommand::Set(key.clone(), value.clone()))
        .map_err(|e| KvsError::RW(e))?;
      self.maybe_compact()?;
      self.kv_cache.insert(key, SeekFrom::Start(seek_pos));
    }
    Ok(())
  }

  /// Remove the value associated with the key from the store
  pub fn remove(&mut self, key: String) -> Result<()> {
    if self.kv_cache.contains_key(&key) {
      serde_json::to_writer(&self.log_file, &LogCommand::Remove(key.clone()))
        .map_err(|e| KvsError::RW(e))?;
      self.kv_cache.remove(&key);
      self.maybe_compact()?;
      Ok(())
    } else {
      Err(KvsError::KeyNotFound(key))
    }
  }

  fn maybe_compact(&mut self) -> Result<()> {
    if self.enable_compaction {
      self.compaction_counter += 1;
      if self.compaction_counter > Self::N_COMPACTION_CHECKS {
        self.compact()?;
        self.compaction_counter = 0;
      }
    }
    Ok(())
  }

  fn compact(&mut self) -> Result<()> {
    let clone_dir = tempfile::tempdir()
      .map_err(|e| KvsError::IO(e))?;
    let mut kvs_clone = KvStore::open_internal(clone_dir.path(), false)?;
    let keys: Vec<String> = self.kv_cache.keys().cloned().collect();
    for k in keys.iter() {
      let v = self.get(k.clone())?
        .expect("this should exist if nobody messed with the file");
      kvs_clone.set(k.clone(), v)?;
    }
    let clone_path = kvs_clone.log_file_path.clone();
    std::fs::remove_file(&self.log_file_path)
      .map_err(|e| KvsError::IO(e))?;
    drop(kvs_clone);
    std::fs::rename(clone_path.as_path(), self.log_file_path.as_path())
      .map_err(|e| KvsError::IO(e))?;
    self.log_file = OpenOptions::new()
      .create(false)
      .read(true)
      .append(true)
      .open(&self.log_file_path)
      .map_err(|e| KvsError::IO(e))?;
    Ok(())
  }
}
