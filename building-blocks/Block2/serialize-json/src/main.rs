extern crate serde;
extern crate serde_json;

use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Debug, PartialEq, Eq)]
enum Direction {
    Left,
    Right,
    Up,
    Down,
}

#[derive(Serialize, Deserialize, Debug, PartialEq, Eq)]
struct Move {
    direction: Direction,
    length: u32,
}

fn main() -> serde_json::Result<()> {
    let mov = Move {
        direction: Direction::Down,
        length: 10,
    };
    println!("{:?}", mov);
    let serialized = serde_json::to_string(&mov)?;
    println!("{}", serialized);
    let deserialized: Move = serde_json::from_str(&serialized)?;
    println!("{:?}", deserialized);
    Ok(())
}
