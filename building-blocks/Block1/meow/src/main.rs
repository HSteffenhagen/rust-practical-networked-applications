#[macro_use]
extern crate clap;

use clap::App;

use std::error;
use std::fmt;

#[derive(Debug)]
struct WrongCommandLineError {
    msg: String
}

impl WrongCommandLineError {
    fn new(msg: &str) -> WrongCommandLineError {
        WrongCommandLineError { msg: String::from(msg) }
    }
}

impl fmt::Display for WrongCommandLineError {
   fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
       write!(f, "Wrong command line: {}", self.msg)
   }
}

impl error::Error for WrongCommandLineError { }

fn main() -> Result<(), Box<dyn error::Error>> {
    let cli_config = load_yaml!("cli.yaml");
    let options = App::from_yaml(cli_config).get_matches();
    match options.value_of("config") {
        Some(file) => {
            println!("File: {}", file);
            Ok(())
        }
        None => Err(Box::new(WrongCommandLineError::new("missing file")))
    }
}
